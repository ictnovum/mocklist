<html>
<head>
<link rel="stylesheet" type="text/css" href=".\style.css">
<title>Mocklist!</title>
</head>
<body>

<?php
$servername = "localhost";
$username = "mocklist";
$password = "msQx58mzi7ywMhW";
$dbname = "mocklist";

// Create connection
$conn = new mysqli($servername, $username, $password,$dbname);

// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

echo '<div class="centerPart">';

echo '<form action="/submit.php" method="post">';

echo '<label for="member">Pick a member: </label>';

echo '<select name="name" id="name"';

echo '<option value="" selected>Pick an Active Member </option>' ;

echo '<option value="" selected>'."Pick an Active Member".'</option>';

// BOARD
 $sql = "SELECT name, committee FROM activeMembers WHERE committee LIKE 'Board' ";
 $result = $conn->query($sql);

 echo '<optgroup label="Board">';
 if ($result->num_rows > 0) {
     // output data of each row
     while($row = $result->fetch_assoc()) {
         echo '<option value="'.$row["name"].'">'.$row["name"].'</option>';
     }
 }
 echo '</optgroup>';

 // ICT
 $sql = "SELECT name, committee FROM activeMembers WHERE committee LIKE 'ICT' ";
 $result = $conn->query($sql);

 echo '<optgroup label="ICT">';
 if ($result->num_rows > 0) {
     // output data of each row
     while($row = $result->fetch_assoc()) {
         echo '<option value="'.$row["name"].'">'.$row["name"].'</option>';
     }
 }
 echo '</optgroup>';

 // Activity
 $sql = "SELECT name, committee FROM activeMembers WHERE committee LIKE 'Activity' ";
 $result = $conn->query($sql);

 echo '<optgroup label="Activity">';
 if ($result->num_rows > 0) {
     // output data of each row
     while($row = $result->fetch_assoc()) {
         echo '<option value="'.$row["name"].'">'.$row["name"].'</option>';
     }
 }
 echo '</optgroup>';

 // Catering
 $sql = "SELECT name, committee FROM activeMembers WHERE committee LIKE 'Catering' ";
 $result = $conn->query($sql);

 echo '<optgroup label="Catering">';
 if ($result->num_rows > 0) {
     // output data of each row
     while($row = $result->fetch_assoc()) {
         echo '<option value="'.$row["name"].'">'.$row["name"].'</option>';
     }
 }
 echo '</optgroup>';

 // External
 $sql = "SELECT name, committee FROM activeMembers WHERE committee LIKE 'Extern' ";
 $result = $conn->query($sql);

 echo '<optgroup label="External">';
 if ($result->num_rows > 0) {
     // output data of each row
     while($row = $result->fetch_assoc()) {
         echo '<option value="'.$row["name"].'">'.$row["name"].'</option>';
     }
 }
 echo '</optgroup>';

 // Student Support
 $sql = "SELECT name, committee FROM activeMembers WHERE committee LIKE 'StudentSupport' ";
 $result = $conn->query($sql);

 echo '<optgroup label="Student Support">';
 if ($result->num_rows > 0) {
     // output data of each row
     while($row = $result->fetch_assoc()) {
         echo '<option value="'.$row["name"].'">'.$row["name"].'</option>';
     }
 }
 echo '</optgroup>';

 // Party
 $sql = "SELECT name, committee FROM activeMembers WHERE committee LIKE 'Party' ";
 $result = $conn->query($sql);

 echo '<optgroup label="Party">';
 if ($result->num_rows > 0) {
     // output data of each row
     while($row = $result->fetch_assoc()) {
         echo '<option value="'.$row["name"].'">'.$row["name"].'</option>';
     }
 }
 echo '</optgroup>';

 // International
 $sql = "SELECT name, committee FROM activeMembers WHERE committee LIKE 'International' ";
 $result = $conn->query($sql);

 echo '<optgroup label="International">';
 if ($result->num_rows > 0) {
     // output data of each row
     while($row = $result->fetch_assoc()) {
         echo '<option value="'.$row["name"].'">'.$row["name"].'</option>';
     }
 }
 echo '</optgroup>';

 // Intro
 $sql = "SELECT name, committee FROM activeMembers WHERE committee LIKE 'Intro' ";
 $result = $conn->query($sql);

 echo '<optgroup label="Intro">';
 if ($result->num_rows > 0) {
     // output data of each row
     while($row = $result->fetch_assoc()) {
         echo '<option value="'.$row["name"].'">'.$row["name"].'</option>';
     }
 }
 echo '</optgroup>';

 // Media
 $sql = "SELECT name, committee FROM activeMembers WHERE committee LIKE 'Media' ";
 $result = $conn->query($sql);

 echo '<optgroup label="Media">';
 if ($result->num_rows > 0) {
     // output data of each row
     while($row = $result->fetch_assoc()) {
         echo '<option value="'.$row["name"].'">'.$row["name"].'</option>';
     }
 }
 echo '</optgroup>';

 // Studytrip
 $sql = "SELECT name, committee FROM activeMembers WHERE committee LIKE 'Studytrip' ";
 $result = $conn->query($sql);

 echo '<optgroup label="Studytrip">';
 if ($result->num_rows > 0) {
     // output data of each row
     while($row = $result->fetch_assoc()) {
         echo '<option value="'.$row["name"].'">'.$row["name"].'</option>';
     }
 }
 echo '</optgroup>';

 // Sound
 $sql = "SELECT name, committee FROM activeMembers WHERE committee LIKE 'Sound' ";
 $result = $conn->query($sql);

 echo '<optgroup label="Sound">';
 if ($result->num_rows > 0) {
     // output data of each row
     while($row = $result->fetch_assoc()) {
         echo '<option value="'.$row["name"].'">'.$row["name"].'</option>';
     }
 }
 echo '</optgroup>';

 // Far Home
 $sql = "SELECT name, committee FROM activeMembers WHERE committee LIKE 'Far Home' ";
 $result = $conn->query($sql);

 echo '<optgroup label="Far Home">';
 if ($result->num_rows > 0) {
     // output data of each row
     while($row = $result->fetch_assoc()) {
         echo '<option value="'.$row["name"].'">'.$row["name"].'</option>';
     }
 }
 echo '</optgroup>';

 // Lustrum
 $sql = "SELECT name, committee FROM activeMembers WHERE committee LIKE 'Lustrum' ";
 $result = $conn->query($sql);

 echo '<optgroup label="Lustrum">';
 if ($result->num_rows > 0) {
     // output data of each row
     while($row = $result->fetch_assoc()) {
         echo '<option value="'.$row["name"].'">'.$row["name"].'</option>';
     }
 }
 echo '</optgroup>';

// Edu
 $sql = "SELECT name, committee FROM activeMembers WHERE committee LIKE 'edu' ";
 $result = $conn->query($sql);

 echo '<optgroup label="Edu">';
 if ($result->num_rows > 0) {
     // output data of each row
     while($row = $result->fetch_assoc()) {
         echo '<option value="'.$row["name"].'">'.$row["name"].'</option>';
     }
 }
 echo '</optgroup>';

 // Lost Persons
 $sql = "SELECT name, committee FROM activeMembers WHERE committee IS NULL ";
 $result = $conn->query($sql);

 echo '<optgroup label="Lost Persons">';
 if ($result->num_rows > 0) {
     // output data of each row
     while($row = $result->fetch_assoc()) {
         echo '<option value="'.$row["name"].'">'.$row["name"].'</option>';
     }
 }
 echo '</optgroup>';

echo '</select>';

echo '<br><br>';

echo '<label for"nickname">Nickname </label>';

echo '<input type="text" id="nickname" name="nickname" value="">';

echo '<br><br>';

echo '<input type="hidden" id="secretValue" name="secretValue" value="31415"> ';

echo '<input type="submit" value="Submit">';

echo '</form>';

echo '<form action="/list.php" method="post" class="form2">';
echo '<input type="submit" value="View the nicknames">';
echo '</form>';

echo '</div>';

$conn->close();

?> 



</body>
</html>
