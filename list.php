<html>
<head>
<title>Mocklist!</title>

<style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}
</style>

</head>
<body>

 <?php
$servername = "localhost";
$username = "mocklist";
$password = "msQx58mzi7ywMhW";
$dbname = "mocklist";

// Create connection
$conn = new mysqli($servername, $username, $password,$dbname);

// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT name, nickname, hide FROM nicknames ORDER BY name ASC";
$result = $conn->query($sql);

echo "<table>";
echo "<tr><th>Name</th><th>Nickname</th></tr>";

if ($result->num_rows > 0) {
  // output data of each row
  while($row = $result->fetch_assoc()) {

  if($row["hide"]==0){

  echo "<tr>";
  echo "<td>".$row["name"]."</td>";
  echo "<td>".$row["nickname"]."</td>";
  echo "</tr>";
}
 }
} else {
  echo "0 results";
}

echo "</table>";

/*

$conn->close();

?> 


</body>
</html>
